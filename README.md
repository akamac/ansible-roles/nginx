## nginx setup for TLS termination

Default values:

```yaml
server_name: "{{ ansible_fqdn }}"
listen_port: 443
cert_dir: /etc/ssl
cert_name: "{{ ansible_hostname }}"
```

Use as dependency in other roles:

```yaml
dependencies:
  - role: nginx
    name: nginx
    src: git+https://gitlab.com/akamac-ansible-roles/nginx.git
    vars:
      nginx_roles: tls-termination
      upstream: 127.0.0.1:{{ service_port }}
```